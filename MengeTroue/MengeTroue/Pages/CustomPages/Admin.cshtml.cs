﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MengeTroue.Data;
using MengeTroue.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MengeTroue.Pages.CustomPages
{
    [Authorize]
    public class AdminModel : PageModel
    {
        ApplicationDbContext _DbContext;
        DbHandler _DbHandler;

        [BindProperty]
        public List<RSVP> AllRSVPS { get; set; }
        [BindProperty]
        public List<Message> AllMessages { get; set; }
        public AdminModel(ApplicationDbContext DbContext)
        {
            _DbHandler =  new DbHandler(DbContext);
        }
        public void OnGet()
        {
            AllRSVPS =  _DbHandler.GetAllRSVPs();
            AllMessages = _DbHandler.GetAllMessages();
        }
    }
}