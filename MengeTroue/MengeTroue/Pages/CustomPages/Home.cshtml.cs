﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MengeTroue.Data;
using MengeTroue.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MengeTroue.Pages.CustomPages
{
    public class HomeModel : PageModel
    {
        [BindProperty]
        public RSVP _RSVP { get; set; }

        [BindProperty]
        public Message _Message { get; set; }

        public ApplicationDbContext _DbContext;
        public DbHandler _DbHandler;
        public EmailHandler _EmailHandler;

        public HomeModel(ApplicationDbContext DbContext)
        {
            _DbContext = DbContext;
            _DbHandler = new DbHandler(_DbContext);
            _EmailHandler = new EmailHandler();
        }

        public async Task<IActionResult> OnPostRSVPAsync()
        {
            await _DbHandler.PostRSVP(_RSVP);
            _EmailHandler.SendRSVP(_RSVP);


            return RedirectToPage("Home");
        }

        public async Task<IActionResult> OnPostMessageAsync()
        {
            await _DbHandler.PostMessage(_Message);
            _EmailHandler.SendMessage(_Message);
            
            return RedirectToPage("Home");
        }


        public void OnGet()
        {
        }
    }
}