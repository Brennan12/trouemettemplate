﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using MengeTroue.Models;

namespace MengeTroue
{


    public class EmailHandler
    {
        static string smtpAddress = "smtp.gmail.com";
        static int portNumber = 587;
        static bool enableSSL = true;
        static string emailFromAddress = "mengetroue@gmail.com"; //Sender Email Address  
        static string password = "yhFL82pbeycfPBv"; //Sender Password  
        static string emailToAddress = "inbox.mengetroue@gmail.com"; //Receiver Email Address  
        public string subject = "";
        public string body = "";

        public void SendRSVP(RSVP rsvp) {

            subject = "RSVP: "+ rsvp.Name;
            body = "Attending: " + rsvp.Attending + Environment.NewLine
            + "Which days: "+rsvp.AttendingDays + Environment.NewLine
            + "Email: "+rsvp.Email + Environment.NewLine
            + "Number: "+rsvp.Number + Environment.NewLine
            + "Song request: "+rsvp.SongRequest + Environment.NewLine
            + "Note: "+rsvp.Note;

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFromAddress);
                mail.To.Add(emailToAddress);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = false;
                //mail.Attachments.Add(new Attachment("D:\\TestFile.txt"));//--Uncomment this to send any attachment  
                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new NetworkCredential(emailFromAddress, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }

        }


        public void SendMessage(Message message) {

            subject = "Message: " + message.Name;

            body = "Name: " + message.Name + Environment.NewLine
                + "Email: " + message.Email + Environment.NewLine
                + "Number: " + message.Number + Environment.NewLine
                + "Message: "+ Environment.NewLine + message.MessageContent;

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFromAddress);
                mail.To.Add(emailToAddress);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = false;
                //mail.Attachments.Add(new Attachment("D:\\TestFile.txt"));//--Uncomment this to send any attachment  
                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new NetworkCredential(emailFromAddress, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }

        }

    }
}


/*
     class Program {  
        static string smtpAddress = "smtp.gmail.com";  
        static int portNumber = 587;  
        static bool enableSSL = true;  
        static string emailFromAddress = "sender@gmail.com"; //Sender Email Address  
        static string password = "Abc@123$%^"; //Sender Password  
        static string emailToAddress = "receiver@gmail.com"; //Receiver Email Address  
        static string subject = "Hello";  
        static string body = "Hello, This is Email sending test using gmail.";  
        static void Main(string[] args) {  
            SendEmail();  
        }  
        public static void SendEmail() {  
            using(MailMessage mail = new MailMessage()) {  
                mail.From = new MailAddress(emailFromAddress);  
                mail.To.Add(emailToAddress);  
                mail.Subject = subject;  
                mail.Body = body;  
                mail.IsBodyHtml = true;  
                //mail.Attachments.Add(new Attachment("D:\\TestFile.txt"));//--Uncomment this to send any attachment  
                using(SmtpClient smtp = new SmtpClient(smtpAddress, portNumber)) {  
                    smtp.Credentials = new NetworkCredential(emailFromAddress, password);  
                    smtp.EnableSsl = enableSSL;  
                    smtp.Send(mail);  
                }  
            }  
        } 
     
     
     */
