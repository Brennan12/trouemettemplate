﻿using System;
using System.Collections.Generic;
using System.Text;
using MengeTroue.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MengeTroue.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<RSVP> RSVPs {get;set;}
        public DbSet<Message> Messages{ get; set; }
    }
}
