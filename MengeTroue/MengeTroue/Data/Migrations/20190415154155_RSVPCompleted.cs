﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MengeTroue.Data.Migrations
{
    public partial class RSVPCompleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Attending",
                table: "RSVPs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttendingDays",
                table: "RSVPs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "RSVPs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Number",
                table: "RSVPs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SongRequest",
                table: "RSVPs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Attending",
                table: "RSVPs");

            migrationBuilder.DropColumn(
                name: "AttendingDays",
                table: "RSVPs");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "RSVPs");

            migrationBuilder.DropColumn(
                name: "Number",
                table: "RSVPs");

            migrationBuilder.DropColumn(
                name: "SongRequest",
                table: "RSVPs");
        }
    }
}
