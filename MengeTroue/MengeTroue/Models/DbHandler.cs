﻿using MengeTroue.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MengeTroue.Models
{
    public class DbHandler
    {
        public ApplicationDbContext _DbContext;
        public DbHandler(ApplicationDbContext DbContext)
        {
            _DbContext = DbContext;
        }

        public async Task PostRSVP(RSVP NewRSVP)
        {
            _DbContext.RSVPs.Add(NewRSVP);
            await _DbContext.SaveChangesAsync();
        }

        public async Task PostMessage(Message NewMessage)
        {
            _DbContext.Messages.Add(NewMessage);
            await _DbContext.SaveChangesAsync();
        }

        public List<RSVP> GetAllRSVPs()
        {
            var test = _DbContext.RSVPs.ToList();
            return _DbContext.RSVPs.ToList();

        }

        public List<Message> GetAllMessages()
        {

            var test = _DbContext.Messages.ToList();
            return _DbContext.Messages.ToList();

        }
    }
}
