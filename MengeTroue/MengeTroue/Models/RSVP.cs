﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace MengeTroue.Models
{
    public class RSVP
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public string Attending { get; set; }
        public string AttendingDays { get; set; }
        public string SongRequest { get; set; }
        public string Note { get; set; }

    }
}
