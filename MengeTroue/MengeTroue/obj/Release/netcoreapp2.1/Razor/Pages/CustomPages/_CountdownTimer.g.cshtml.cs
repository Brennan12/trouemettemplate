#pragma checksum "D:\Personal\Coding\PaulTroue\template\TroueMetTemplate\MengeTroue\MengeTroue\Pages\CustomPages\_CountdownTimer.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7067773a7515d8c09a79a841177bc3b9f8a2745b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(MengeTroue.Pages.CustomPages.Pages_CustomPages__CountdownTimer), @"mvc.1.0.view", @"/Pages/CustomPages/_CountdownTimer.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Pages/CustomPages/_CountdownTimer.cshtml", typeof(MengeTroue.Pages.CustomPages.Pages_CustomPages__CountdownTimer))]
namespace MengeTroue.Pages.CustomPages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Personal\Coding\PaulTroue\template\TroueMetTemplate\MengeTroue\MengeTroue\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "D:\Personal\Coding\PaulTroue\template\TroueMetTemplate\MengeTroue\MengeTroue\Pages\_ViewImports.cshtml"
using MengeTroue;

#line default
#line hidden
#line 3 "D:\Personal\Coding\PaulTroue\template\TroueMetTemplate\MengeTroue\MengeTroue\Pages\_ViewImports.cshtml"
using MengeTroue.Data;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7067773a7515d8c09a79a841177bc3b9f8a2745b", @"/Pages/CustomPages/_CountdownTimer.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"937c8bcb1a082f467746183c6c1f08e2017c7b5a", @"/Pages/_ViewImports.cshtml")]
    public class Pages_CustomPages__CountdownTimer : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(121, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(130, 917, true);
            WriteLiteral(@"
<style>
    .CountdownTimer {
        position: relative;
        height: 35vh;
        width: 100vw;
        background-color: #b1b881;
        align-content: center;
        margin: 0px;
    }

    .CountdownTimerBlocks {
        width: 25%;
        height: 100%;
        margin: auto;
        padding: 1vw;
    }

    .CombinedCountdownBlocks {
        top: 5vh;
        height: 70%;
        margin: auto;
        margin-top: 4vh;
    }

    .CountdownNumbers {
        font-size: 5vw;
        height: 100%;
        margin: auto;
        margin-top: 0vh;
        color: #e6e6e6;
    }

    .CountdownTitle {
        color: #d7d7d7;
        font-size: 2vw;
    }

    .CountdownUnits {
        margin: 3vw;
        margin-top: 1vh;
        color: #d7d7d7;
        font-size: 2vw;
        left: 50%;
    }

    .CountdownTitleDiv {
        margin: auto;
    }

    ");
            EndContext();
            BeginContext(1048, 4399, true);
            WriteLiteral(@"@media only screen and (max-width: 600px) {


        .CountdownTimer {
            position: relative;
            height: 72vw;
            width: 100vw;
            background-color: #b1b881;
            align-content: center;
        }

        .CountdownTimerBlocks {
            width: 25%;
            height: 100%;
            margin: auto;
            padding: 2vw;
        }

        .CombinedCountdownBlocks {
            top: 10vw;
            height: 70%;
            margin: auto;
            margin-top: 10vw;
        }

        .CountdownNumbers {
            font-size: 7vw;
            height: 100%;
            margin: auto;
            color: #e6e6e6;
        }

        .CountdownTitle {
            color: #d7d7d7;
            font-size: 5vw;
            margin-bottom:5vw;
        }

        .CountdownUnits {
            margin: 3vw;
            margin-top: 0vw;
            color: #d7d7d7;
            font-size: 4vw;
            left: 50%;
        }

 ");
            WriteLiteral(@"       .CountdownTitleDiv {
            margin: auto;
        }
    }
</style>


<div class=""CountdownTimer"" style=""text-align: center;"">
    <div class=""row"" style="" height:100%;margin: auto;"">
        <div class=""CombinedCountdownBlocks"">
            <div class=""row"">
                <div class=""CountdownTitleDiv"">
                    <label class=""CountdownTitle"">THE WEDDING BEGINS IN</label>
                </div>
            </div>
            <div class=""row"" style=""height:50%;"">
                <div class=""column CountdownTimerBlocks text-center"">
                    <label id=""CountdownDays"" class=""CountdownNumbers"">00</label>
                </div>
                <div class=""column CountdownTimerBlocks text-center"">
                    <label id=""CountdownHours"" class=""CountdownNumbers"">00</label>
                </div>
                <div class=""column CountdownTimerBlocks text-center"">
                    <label id=""CountdownMinutes"" class=""CountdownNumbers"">00</label>
   ");
            WriteLiteral(@"             </div>
                <div class=""column CountdownTimerBlocks text-center"">
                    <label id=""CountdownSeconds"" class=""CountdownNumbers"">00</label>
                </div>
            </div>
            <div class=""row"" style=""height:30%;"">
                <div class=""column CountdownTimerBlocks"">
                    <label class=""CountdownUnits"">Days</label>
                </div>
                <div class=""column CountdownTimerBlocks"">
                    <label class=""CountdownUnits"">Hours</label>
                </div>
                <div class=""column CountdownTimerBlocks"">
                    <label class=""CountdownUnits"">Minutes</label>
                </div>
                <div class=""column CountdownTimerBlocks"">
                    <label class=""CountdownUnits"">Seconds</label>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    var DaysLabel = document.getElementById('CountdownDays');
    var HoursLabel = do");
            WriteLiteral(@"cument.getElementById('CountdownHours');
    var MinutesLabel = document.getElementById('CountdownMinutes');
    var SecondsLabel = document.getElementById('CountdownSeconds');

    // Set the date we're counting down to
    var countDownDate = new Date(""Sept 7, 2019 15:37:25"").getTime();
    // Update the count down every 1 second
    var x = setInterval(function () {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id=""demo""
        DaysLabel");
            WriteLiteral(@".innerHTML = days;
        HoursLabel.innerHTML = hours;
        MinutesLabel.innerHTML = minutes;
        SecondsLabel.innerHTML = seconds;

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);

        }
    }, 1000);
</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
