#pragma checksum "D:\Personal\Coding\PaulTroue\template\TroueMetTemplate\MengeTroue\MengeTroue\Pages\CustomPages\_RSVP.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8b0dd10968025cfa96d580edbe10dbc6dfb17bfa"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(MengeTroue.Pages.CustomPages.Pages_CustomPages__RSVP), @"mvc.1.0.view", @"/Pages/CustomPages/_RSVP.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Pages/CustomPages/_RSVP.cshtml", typeof(MengeTroue.Pages.CustomPages.Pages_CustomPages__RSVP))]
namespace MengeTroue.Pages.CustomPages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Personal\Coding\PaulTroue\template\TroueMetTemplate\MengeTroue\MengeTroue\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "D:\Personal\Coding\PaulTroue\template\TroueMetTemplate\MengeTroue\MengeTroue\Pages\_ViewImports.cshtml"
using MengeTroue;

#line default
#line hidden
#line 3 "D:\Personal\Coding\PaulTroue\template\TroueMetTemplate\MengeTroue\MengeTroue\Pages\_ViewImports.cshtml"
using MengeTroue.Data;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8b0dd10968025cfa96d580edbe10dbc6dfb17bfa", @"/Pages/CustomPages/_RSVP.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"937c8bcb1a082f467746183c6c1f08e2017c7b5a", @"/Pages/_ViewImports.cshtml")]
    public class Pages_CustomPages__RSVP : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(121, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(130, 1287, true);
            WriteLiteral(@"<style>

    .RSVPDiv {
        height: 95vh;
        margin: auto;
        margin-top: 20px;
        width: 30vw;
        border: 3px solid #b1b881;
        padding: 10px;
    }

    .RSVPTitleDiv {
        height: 5vh;
        margin: auto;
        margin-top: 3vh;
        width: 30vw;
        padding: 10px;
    }

    .RSVPTitleLabel {
        font-size: 2vw;
        height: 100%;
        margin: auto;
        margin-top: 2vh;
        color: #b1b881;
    }

    .RSVPSubTitleLabel {
        font-size: 1.15vw;
        height: 100%;
        margin: auto;
        margin-top: 2.5vh;
        color: #d7d7d7;
    }


    .RSVPInputDiv {
        width: 90%;
        margin: auto;
    }

    .RSVPInputLabels {
        margin: 1vw;
        margin-bottom: 0px;
    }

    .RSVPInputs {
        width: 100%;
        margin: 1vw;
        height: 7%;
        margin-top: 1vh;
        border: 3px solid #b1b881;
        font-size: 1.25vw;
        padding: 1vw;
    }

    ");
            WriteLiteral(@".RSVPSubmitButton {
        height: 7%;
        width: 25%;
        margin-top: 5%;
        border: 3px solid #b1b881;
        border-radius: 15px;
        background-color: #b1b881;
    }

    .RSVPSubmitButtonSpan {
        color: #FFF;
    }

    ");
            EndContext();
            BeginContext(1418, 4728, true);
            WriteLiteral(@"@media only screen and (max-width: 600px) {



        .RSVPDiv {
            height: 80vh;
            margin: auto;
            margin-top: 20px;
            width: 95vw;
            border: 3px solid #b1b881;
            padding: 10px;
        }

        .RSVPTitleDiv {
            height: 5vh;
            margin: auto;
            margin-top: 3vh;
            width: 60vw;
            padding: 10px;
        }

        .RSVPTitleLabel {
            font-size: 7vw;
            height: 100%;
            margin: auto;
            margin-top: 2vh;
            color: #b1b881;
        }

        .RSVPSubTitleLabel {
            font-size: 4vw;
            height: 100%;
            margin: auto;
            margin-top: 2vh;
            color: #d7d7d7;
        }


        .RSVPInputDiv {
            width: 90%;
            margin: auto;
        }

        .RSVPInputLabels {
            margin: 1vw;
            margin-bottom: 0px;
        }

        .RSVPInputs {
    ");
            WriteLiteral(@"        width: 100%;
            margin: 1vw;
            height: 7%;
            margin-top: 1vh;
            border: 3px solid #b1b881;
            font-size: 1.25vw;
            padding: 1vw;
        }

        .RSVPSubmitButton {
            height: 7%;
            width: 25%;
            margin-top: 5%;
            border: 3px solid #b1b881;
            border-radius: 15px;
            background-color: #b1b881;
        }

        .RSVPSubmitButtonSpan {
            color: #FFF;
        }
    }
</style>
<meta name=""viewport"" content=""width=device-width, initial-scale=1"" />

<div class=""row"" style=""height:150px;"">

    <div class=""RSVPTitleDiv"">

        <div class=""row"" style=""height:50px;"">
            <label class=""RSVPTitleLabel""> Will you attend?</label>
        </div>
        <div class=""row"" style=""height:50px;"">
            <label class=""RSVPSubTitleLabel""> Please sign your RSVP</label>
        </div>
    </div>
</div>

<div class=""RSVPDiv"">

    <div class=");
            WriteLiteral(@"""row"">
        <div class=""RSVPInputDiv"">
            <div class=""row"">
                <label class=""RSVPInputLabels"">YOUR NAME</label>
            </div>
            <div class=""row"">
                <input type=""text"" class=""RSVPInputs"" id=""pwd"">
            </div>
        </div>
    </div>

    <div class=""row"">
        <div class=""RSVPInputDiv"">
            <div class=""row"">
                <label class=""RSVPInputLabels"">YOUR NUMBER</label>
            </div>
            <div class=""row"">
                <input type=""text"" class=""RSVPInputs"" id=""pwd"">
            </div>
        </div>
    </div>

    <div class=""row"">
        <div class=""RSVPInputDiv"">
            <div class=""row"">
                <label class=""RSVPInputLabels"">YOUR EMAIL</label>
            </div>
            <div class=""row"">
                <input type=""text"" class=""RSVPInputs"" id=""pwd"">
            </div>
        </div>
    </div>

    <div class=""row"">
        <div class=""RSVPInputDiv"" style=""margin");
            WriteLiteral(@"-left:10%;"">
            <div class=""row"">
                <label class=""RSVPInputLabels"" style=""font-size:20px; margin-left:-1%;"">Will you attend?</label>
            </div>

            <div class=""row"">
                <div class=""checkbox "" style=""margin-top:15px;"">
                    <label><input type=""checkbox"" class=""RSVPCheckboxes"" value="""">Yes, I'll be there</label>
                </div>
            </div>

            <div class=""row"">
                <div class=""checkbox "" style=""margin-top:0px;"">
                    <label><input type=""checkbox"" class=""RSVPCheckboxes"" value="""">Sorry, I can't come</label>
                </div>

            </div>



        </div>
    </div>

    <div class=""row"">
        <div class=""RSVPInputDiv"">
            <div class=""row"">
                <label class=""RSVPInputLabels"">SONG REQUEST (Optional)</label>
            </div>
            <div class=""row"">
                <input type=""text"" class=""RSVPInputs"" id=""pwd"">
            </d");
            WriteLiteral(@"iv>
        </div>
    </div>

    <div class=""row"">
        <div class=""RSVPInputDiv"">
            <div class=""row"">
                <label class=""RSVPInputLabels"">NOTE</label>
            </div>
            <div class=""row"">
                <input type=""text"" class=""RSVPInputs"" id=""pwd"">
            </div>
        </div>
    </div>

    <div class=""row"">
        <div class=""RSVPInputDiv"">
            <button class=""RSVPSubmitButton"">

                <span class=""RSVPSubmitButtonSpan"">
                    SEND RSVP
                </span>

            </button>
        </div>
    </div>


</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
